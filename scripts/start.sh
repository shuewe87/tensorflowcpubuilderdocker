#!/bin/bash
git clone https://github.com/tensorflow/tensorflow
cd tensorflow
if [ -n "$@" ]; then
 tag="$@"
else
 tag="v1.6.0"
fi
git checkout $tag
touch WORKSPACE
#Needs input..

export PYTHON_BIN_PATH=/usr/bin/python
export PYTHON_LIB_PATH=/usr/lib/python3/dist-packages
export TF_NEED_GCP=0 
export TF_NEED_CUDA=0 
export TF_NEED_HDFS=0
export TF_NEED_OPENCL=0
export TF_NEED_JEMALLOC=0
export TF_ENABLE_XLA=0
export TF_NEED_VERBS=0
export TF_NEED_MKL=0
export TF_NEED_S3=0
export TF_NEED_KAFKA=0
export TF_NEED_GDR=0
export TF_NEED_MPI=0
export TF_NEED_OPENCL_SYCL=0
export CC_OPT_FLAGS=-march=native
export TF_SET_ANDROID_WORKSPACE=0
./configure


bazel build --config=opt //tensorflow/tools/pip_package:build_pip_package
bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg

if [ ! -e /output/ ]; then
 mkdir /output
fi
cp -r /tmp/tensorflow_pkg/ /output/

echo ""
echo "Wheele was saved to output folder"
exec /bin/bash
